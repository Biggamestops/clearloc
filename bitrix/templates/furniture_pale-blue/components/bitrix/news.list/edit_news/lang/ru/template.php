<?php
$MESS['ADD'] = 'Добавить';
$MESS['EDIT'] = 'Редактировать';
$MESS['DELETE'] = 'Удалить';
$MESS['MODAL_EDIT_ADD'] = 'Добавить / редактировать новости';
$MESS['NAME_NEWS'] = 'Название';
$MESS['TEXT_NEWS'] = 'Описание';
$MESS['SAVE'] = 'Сохранить';