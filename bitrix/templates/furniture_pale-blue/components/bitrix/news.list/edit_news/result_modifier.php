<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $USER;
$arFilter = array("ID" => $USER->GetID());
$arParams["SELECT"] = array("UF_PREMISSION");
$arRes = CUser::GetList($by,$desc,$arFilter,$arParams);
if ($res = $arRes->Fetch()) {
    foreach ($res["UF_PREMISSION"] as $id) {
        $rsRes= CUserFieldEnum::GetList(array(), array(
            "ID" => $id,
        ));
        if($arGender = $rsRes->GetNext()) {
            if ($arGender['XML_ID'] == 'EDIT') {
                $arResult['SHOW_EDIT'] = true;
            }
            if ($arGender['XML_ID'] == 'ADD') {
                $arResult['SHOW_ADD'] = true;
            }
            if ($arGender['XML_ID'] == 'DELETE') {
                $arResult['SHOW_DELETE'] = true;
            }
        }
    }
}