<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?php $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();
$bAjax = $request->get('ajax') == 'y';?>
<div class="js-news-list">
    <?php if ($arResult['SHOW_ADD'] && !$bAjax):?>
        <div class="row">
            <div class="col-md-12">
                <button data-iblock-id="<?php echo $arParams['IBLOCK_ID']?>" data-action="add" class="js-action btn btn-primary" data-toggle="modal" data-target="#modalEditAddNews"><?php echo GetMessage('ADD')?></button>
            </div>
        </div>
    <?php endif?>
    <?php include 'include/news.php'?>
</div>
<?php if (!$bAjax):?>
    <?php include 'include/modal.php'?>
<?php endif;?>

