<div id="modalEditAddNews" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <form name="edit_add_news" class="js-form-add-edit-news">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><?php echo GetMessage('MODAL_EDIT_ADD')?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="message-ouput"></div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><?php echo GetMessage('NAME_NEWS')?></span>
                        </div>
                        <input name="NAME_NEWS" value="" type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><?php echo GetMessage('TEXT_NEWS')?></span>
                        </div>
                        <textarea name="TEXT_NEWS" class="form-control" aria-label="With textarea"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary"><?php echo GetMessage('SAVE')?></button>
                </div>
            </div>
        </form>
    </div>
</div>