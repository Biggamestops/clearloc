$(function(){
   var $body = $('body');

   var updateList = function(){
       $.ajax({
           url: "/ajax/news_list.php",
           type: "POST",
           dataType: "html",
           data: {'ajax':'y'},
           success:function(resp){
               if (resp.length) {
                   $('.js-news-list').html(resp);
               }
           }
       });
   };

   $body.on('show.bs.modal',"#modalEditAddNews",function () {
       var $this = $(this);
       $this.find('.message-ouput').hide();
       if ($this.data('action') != 'edit') return;

       $this.find('input,textarea').addClass('disabled');
       var data = {'ajax':'y','action': 'get_info', 'ID': $this.data('product-id'), 'IBLOCK_ID':$this.data('iblock-id')};
       $.ajax({
           url: "/ajax/news_proccess.php",
           type: "POST",
           dataType: "json",
           data: data,
           success:function(resp){
               if (resp.success) {
                   $this.find('[name="NAME_NEWS"]').val(resp.name_news);
                   $this.find('[name="TEXT_NEWS"]').val(resp.text_news);
               } else {
                   console.log(resp.message);
               }
               $this.find('input,textarea').removeClass('disabled');
           }
       });
   });

   $body.on('submit','.js-form-add-edit-news',function (e) {
       e.preventDefault();
       var $this = $(this),
            $name = $this.find('[name="NAME_NEWS"]'),
            $text = $this.find('[name="TEXT_NEWS"]');

       $this.find('input,textarea').addClass('disabled');
       var data = {
           'ajax':'y',
           'action': $this.data('action'),
           'ID': $this.data('product-id'),
           'IBLOCK_ID':$this.data('iblock-id'),
           'NAME_NEWS': $name.val(),
           'TEXT_NEWS': $text.val(),
       };
       $.ajax({
           url: "/ajax/news_proccess.php",
           type: "POST",
           dataType: "json",
           data: data,
           success:function(resp){
               if (resp.success) {
                   $this.find('.message-ouput').text(resp.message);
               } else {
                   console.log(resp.message);
               }
               $this.find('.message-ouput').show();
               $this.find('input,textarea').removeClass('disabled');
               updateList();
           }
       });
   });

   $body.on('click','.js-action',function (e) {
       e.preventDefault();
       var $this = $(this),
            $modal = $('#modalEditAddNews'),
            $form = $modal.find('form'),
            iblockId = $this.closest('.news-item').data('iblock-id') ? $this.closest('.news-item').data('iblock-id') : $this.data('iblock-id');
       $modal.data('action',$this.data('action'));
       $modal.data('product-id',$this.closest('.news-item').data('id'));
       $modal.data('iblock-id',iblockId);

       $form.data('action',$this.data('action'));
       $form.data('product-id',$this.closest('.news-item').data('id'));
       $form.data('iblock-id',iblockId);
   });

   $body.on('click','.js-delete',function (e) {
        var $this = $(this),
            $itemData = $this.closest('.news-item');

       $this.addClass('disabled');
       var data = {'ajax':'y','action': $this.data('action'), 'ID': $itemData.data('id')};
       $.ajax({
           url: "/ajax/news_proccess.php",
           type: "POST",
           dataType: "json",
           data: data,
           success:function(resp){
               if (resp.success) {
                   $itemData.remove();
               } else {
                   console.log(resp.message);
               }
           }
       });
   });
});