document.addEventListener("DOMContentLoaded", function(event) {
    window.onload=function(){
        var tred = document.querySelector(".form_block");
        var simple = document.querySelector(".switch-simple-link");
        var extended = document.querySelector(".switch-ex-link");
        var wrap = document.querySelector(".wrap-block");


        extended.addEventListener("click", function(evt) {
            evt.preventDefault();
            tred.classList.add("form-extended");
            extended.classList.add("switch-link-active");
            simple.classList.remove("switch-link-active");
            wrap.classList.remove("wrap-block-mb");
        });

        simple.addEventListener("click", function(evt) {
            evt.preventDefault();
            tred.classList.remove("form-extended");
            simple.classList.add("switch-link-active");
            extended.classList.remove("switch-link-active");
            wrap.classList.add("wrap-block-mb");
        });
    }
});