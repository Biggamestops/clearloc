<?php
include_once "include_stop_statistic.php";
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
include($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/lang/' . LANGUAGE_ID . '/main.php');

use Bitrix\Main\Loader;

$arResult = array(
    'success' => 0
);

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

if ($request->get('ajax') != 'y') return json_encode($arResult);

Loader::includeModule('iblock');

$checkIDFromRequest = function () use ($request, &$arResult){
    $ID = $request->get('ID');
    if (empty($ID)){
        $arResult['message'] = 'empty ID';
        return false;
    }
    return true;
};

$ID = $request->get('ID');

switch ($request->get('action')){
    case 'get_info':

        if (!$checkIDFromRequest()){
            return json_encode($arResult);
        }
        $arElement = CIBlockElement::GetByID($ID)->Fetch();
        $arResult['name_news'] = $arElement['NAME'];
        $arResult['text_news'] = $arElement['PREVIEW_TEXT'] ? $arElement['PREVIEW_TEXT'] : $arElement['DETAIL_TEXT'];
        $arResult['success'] = 1;
    break;

    case 'delete':
        if (!$checkIDFromRequest()){
            echo json_encode($arResult);
        }

        global $DB;
        $DB->StartTransaction();
        if(!CIBlockElement::Delete($ID))
        {
            $arResult['message'] = 'problem with delete';
            $DB->Rollback();
            echo json_encode($arResult);
        }
        else{
            $DB->Commit();
        }
        $arResult['success'] = 1;
    break;

    case 'add':
        $arFields = array(
            'NAME' => $request->get('NAME_NEWS'),
            'PREVIEW_TEXT' => $request->get('TEXT_NEWS'),
            'IBLOCK_ID' => $request->get('IBLOCK_ID')
        );
        $newElement = new CIBlockElement();
        if (!$newElement->Add($arFields)){
            $arResult['message'] = 'problem with add element';
            echo json_encode($arResult);
        }
        $arResult['success'] = 1;
        $arResult['message'] = GetMessage('SUCCESS_ADD');
    break;

    case 'edit':
        if (!$checkIDFromRequest()){
            return json_encode($arResult);
        }

        $arFields = array(
            'NAME' => $request->get('NAME_NEWS'),
            'PREVIEW_TEXT' => $request->get('TEXT_NEWS'),
            'IBLOCK_ID' => $request->get('IBLOCK_ID')
        );
        $newElement = new CIBlockElement();
        if (!$newElement->Update($ID,$arFields)){
            $arResult['message'] = 'problem with update element';
            echo json_encode($arResult);
        }
        $arResult['success'] = 1;
        $arResult['message'] = GetMessage('SUCCESS_EDIT');

    break;
}

echo json_encode($arResult);

include_once ($_SERVER['DOCUMENT_ROOT'] .'/bitrix/modules/main/include/epilog_after.php');