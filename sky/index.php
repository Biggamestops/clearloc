<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Тестовое задание для скайвеб");
global $USER;

if (!$USER->IsAuthorized()):
?>
<?$APPLICATION->IncludeComponent("bitrix:system.auth.form","",Array(
        "REGISTER_URL" => "register.php",
        "FORGOT_PASSWORD_URL" => "",
        "PROFILE_URL" => "profile.php",
        "SHOW_ERRORS" => "Y"
    )
);?>
<?
else:
    $APPLICATION->IncludeFile(
        SITE_DIR."include/news_sky.php",
        Array(),
        Array()
    );
endif;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");