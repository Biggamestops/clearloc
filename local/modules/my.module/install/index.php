<?

use Bitrix\Main\Application;

class my_module extends CModule
{
    var $MODULE_ID = 'my.module';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;

    function __construct()
    {
        \Bitrix\Main\Localization\Loc::loadLanguageFile(__FILE__);
        $arModuleVersion = array();
        include(__DIR__ . "/version.php");
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_NAME = GetMessage('RZ_MODULE_NAME');
        $this->MODULE_DESCRIPTION = GetMessage('RZ_MODULE_DESC');

        $this->PARTNER_NAME = GetMessage('RZ_PARTNER_NAME');
        $this->PARTNER_URI = GetMessage('RZ_PARTNER_URI');
    }

    function InstallEvents()
    {
        return true;
    }

    function UnInstallEvents()
    {
        return true;
    }

    function InstallFiles()
    {
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/admin')) {
            if ($dir = opendir($p)) {
                while (false !== $item = readdir($dir)) {
                    if ($item == '..' || $item == '.' || $item == 'menu.php') {
                        continue;
                    }
                    file_put_contents($file = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $item,
                        '<' . '? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/' . $this->MODULE_ID . '/admin/' . $item . '");?' . '>');
                }
                closedir($dir);
            }
        }
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/install/components')) {
            if ($dir = opendir($p)) {
                while (false !== $item = readdir($dir)) {
                    if ($item == '..' || $item == '.') {
                        continue;
                    }
                    CopyDirFiles($p . '/' . $item, $_SERVER['DOCUMENT_ROOT'] . '/bitrix/components/' . $item, $ReWrite = true,
                        $Recursive = true);
                }
                closedir($dir);
            }
        }
        $jsPath = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/js/' . str_replace('yenisite.', '', $this->MODULE_ID);
        if (!is_dir($jsPath)) {
            mkdir($jsPath, BX_DIR_PERMISSIONS, true);
        }
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/admin/link/')) {
            symlink($p, $jsPath . '/link');
        }
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]. BX_ROOT .'/modules/' . $this->MODULE_ID . '/install/js', $_SERVER["DOCUMENT_ROOT"]. BX_ROOT . "/js/". $this->MODULE_ID , true, true);
        return true;
    }

    function InstallDB()
    {
        return true;
    }


    function UnInstallFiles()
    {
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/admin')) {
            if ($dir = opendir($p)) {
                while (false !== $item = readdir($dir)) {
                    if ($item == '..' || $item == '.') {
                        continue;
                    }
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/' . $this->MODULE_ID . '_' . $item);
                }
                closedir($dir);
            }
        }
        if (is_dir($p = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/' . $this->MODULE_ID . '/install/components')) {
            if ($dir = opendir($p)) {
                while (false !== $item = readdir($dir)) {
                    if ($item == '..' || $item == '.' || !is_dir($p0 = $p . '/' . $item)) {
                        continue;
                    }

                    $dir0 = opendir($p0);
                    while (false !== $item0 = readdir($dir0)) {
                        if ($item0 == '..' || $item0 == '.') {
                            continue;
                        }
                        DeleteDirFilesEx('/bitrix/components/' . $item . '/' . $item0);
                    }
                    closedir($dir0);
                }
                closedir($dir);
            }
        }
        DeleteDirFilesEx('/bitrix/js/' . str_replace('yenisite.', '', $this->MODULE_ID));
        return true;
    }

    function DoInstall()
    {
        $this->InstallEvents();
        $this->InstallFiles();
        $this->InstallDB();
        RegisterModule($this->MODULE_ID);

        return true;
    }

    function DoUninstall()
    {
        if (\Bitrix\Main\ModuleManager::isModuleInstalled($this->MODULE_ID)) {
            $this->UnInstallEvents();
            $this->UnInstallFiles();
            $this->UnInstallDB();
            UnRegisterModule($this->MODULE_ID);
        }
    }
}