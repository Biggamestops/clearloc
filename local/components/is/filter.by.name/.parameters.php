<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS"  =>  array(
        "FILTERS_NAMES"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("FILTERS_NAMES"),
            "TYPE" => "STRING",
            'DEFAULT' => 'arrFilterMain,arrFilterSecond'
        ),
        "FILTERS_LABELS"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("FILTERS_LABELS"),
            "TYPE" => "STRING",
        ),
    )
);