<?
use Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Application;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('iblock'))
{
    ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
    return;
}

class IsFitler extends \CBitrixComponent
{
    private $arFilters;

    public function onPrepareComponentParams($params)
    {
        if ($params['CACHE_TYPE'] == 'Y' || ($params['CACHE_TYPE'] == 'A' && Bitrix\Main\Config\Option::get('main', 'component_cache_on', 'Y') == 'Y')) {
            $params['CACHE_TIME'] = intval($params['CACHE_TIME']);
        } else {
            $params['CACHE_TIME'] = 0;
        }

        if (empty($params['FILTERS_NAMES'])){
            $params['FILTERS_NAMES'] = 'arrFilterMain,arrFilterSecond';
        }

        return $params;
    }

    private function setArFilters(){
        if ($this->request->get('is_filter_submit') != 'Y') return;

       foreach ($this->arFilters as $arFilter){
           if (!$this->request->get($arFilter)) continue;
           $GLOBALS[$arFilter] = array('NAME' => $this->request->get($arFilter).'%');
       }
    }

    private function getArFilters(){
        $strParseFilters = str_replace(' ','',$this->arParams['FILTERS_NAMES']);
        $this->arFilters = explode(',',$strParseFilters);
        $this->arResult['FILTERS'] = $this->arFilters;

        if (!empty($this->arParams['FILTERS_LABELS'])){
            $this->arResult['FILTERS_LABLES'] = explode(',',$this->arParams['FILTERS_LABELS']);
        }
    }

    private function prepareActionForm(){
        $paramsToDelete = array("set_filter", "del_filter", "ajax", "bxajaxid", "AJAX_CALL", "mode");

        foreach($this->arFilters as $arFilter)
        {
            $paramsToDelete[] = $arFilter;
        }

        $this->arResult['ACTION_FORM'] = CHTTP::urlDeleteParams($pageURL, $paramsToDelete, array("delete_system_params" => true));
    }


    public function executeComponent()
    {

        $this->getArFilters();
        $this->setArFilters();
        $this->prepareActionForm();
        $this->includeComponentTemplate();
    }
}