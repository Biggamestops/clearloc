<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$this->setFrameMode(true);
if (empty($arResult['FILTERS'])) return;
global $APPLICATION;
?>

<div class="is-filter-block">
    <form name="<?php echo unserialize($arResult['FILTERS']).'_is_filter'?>" action="<?php echo $arResult['ACTION_FORM'];?>">
        <?php foreach ($arResult['FILTERS'] as $key => $arFilter):?>
            <label>
                <?php echo $arResult['FILTERS_LABLES'][$key]?>
                <input type="text" name="<?php echo $arFilter?>" value="<?php echo $_REQUEST[$arFilter]?>">
            </label>
        <?php endforeach;?>
        <button type="submit" name="is_filter_submit" value="Y"><?php echo GetMessage('SUMBIT')?></button>
    </form>
</div>


