<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS"  =>  array(
        "MAIN_IBLOCK_ID"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("MAIN_IBLOCK_ID"),
            "TYPE" => "STRING",
        ),
        "SECOND_IBLOCK_ID"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("SECOND_IBLOCK_ID"),
            "TYPE" => "STRING",
        ),
        "MAIN_FILTER_NAME"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("MAIN_FILTER_NAME"),
            "TYPE" => "STRING",
            "DEFAULT" => "arrFilterMain",
        ),
        "SECOND_FILTER_NAME"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("SECOND_FILTER_NAME"),
            "TYPE" => "STRING",
            "DEFAULT" => "arrFilterSecond",
        ),
        "PROPERTY_LINK"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("PROPERTY_LINK"),
            "TYPE" => "STRING",
            "DEFAULT" => "PUNKT_HREF",
        ),
        "SHOW_CNT_ELEMENT"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("SHOW_CNT_ELEMENT"),
            "TYPE" => "STRING",
            'DEFAULT' => '3'
        ),
        "CACHE_TIME" => Array("DEFAULT"=>"36000"),
    )
);