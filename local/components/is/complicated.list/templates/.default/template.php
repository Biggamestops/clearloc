<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
$this->setFrameMode(true);
if (empty($arResult['ITEMS'])) return;

foreach ($arResult['ITEMS'] as $arItem):?>
   <div class="main-item">
       <?php echo $arItem['NAME']?>
       <?php foreach ($arItem['SUB_ITEMS'] as $arSubItem):?>
           <div class="sub-item"><?php echo $arSubItem['NAME']?></div>
        <?php endforeach;?>
   </div>
<?php endforeach;?>
<?echo $arResult['NAV_STRING']?>


