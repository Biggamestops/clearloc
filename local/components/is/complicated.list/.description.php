<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("CATALOG_SECTION_PROXY_NAME"),
	"DESCRIPTION" => GetMessage("CATALOG_SECTION_PROXY_DESCRIPTION"),
	"CACHE_PATH" => "Y",
	"SORT" => 70,
	"PATH" => array(
		"ID" => "is",
		"NAME" => GetMessage("COMPONENTS"),
		"CHILD" => array(
			"ID" => "is_test",
			"NAME" => GetMessage("CORE"),
			"SORT" => 30
				),
			),
		);
?>