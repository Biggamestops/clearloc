<?
use Bitrix\Main\Localization\Loc;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

Loc::loadMessages(__FILE__);

if (!\Bitrix\Main\Loader::includeModule('iblock'))
{
    ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALLED'));
    return;
}

class IsComplicatedList extends \CBitrixComponent
{
    private $arrErrors;
    private $arrFilterMain;
    private $arrFilterSecond;
    private $arNavParams;
    private $arNavigation;

    public function onPrepareComponentParams($params)
    {
        if ($params['CACHE_TYPE'] == 'Y' || ($params['CACHE_TYPE'] == 'A' && Bitrix\Main\Config\Option::get('main', 'component_cache_on', 'Y') == 'Y')) {
            $params['CACHE_TIME'] = intval($params['CACHE_TIME']);
        } else {
            $params['CACHE_TIME'] = 0;
        }

        $params['SHOW_CNT_ELEMENT'] = $params['SHOW_CNT_ELEMENT'] ?: 3;
        $params['MAIN_FILTER_NAME'] = $params['MAIN_FILTER_NAME'] ?: 'arrFilterMain';
        $params['SECOND_FILTER_NAME'] = $params['SECOND_FILTER_NAME'] ?: 'arrFilterSecond';

        if (empty($params['MAIN_IBLOCK_ID']) || empty($params['SECOND_IBLOCK_ID']) || empty($params['PROPERTY_LINK'])) {
            $this->arrErrors[] = GetMessage('NOT_FULL_PARAMS');
            return;
        }

        return $params;
    }

    private function getListOfElements(){
        $this->arResult["ITEMS"] = array();
        $arMainElementsIDs = array();

        $dbMainElements = \CIBlockElement::GetList(array(), $this->arrFilterMain , false, $this->arNavParams, array('ID','IBLOCK_ID','NAME'));

        while ($arMainElements = $dbMainElements->Fetch()){
            $arMainElementsIDs[] = $arMainElements['ID'];
            $this->arResult["ITEMS"][$arMainElements['ID']] = $arMainElements;
        }

        if (!$arMainElementsIDs){
            $this->arrErrors[] = GetMessage('EMPTY_MAIN_IBLOCK');
            return;
        }

        $this->arrFilterSecond['PROPERTY_'.$this->arParams['PROPERTY_LINK']] = $arMainElementsIDs;

        $dbSecondsElements = \CIBlockElement::GetList(array(), $this->arrFilterSecond , false, false, array('ID','IBLOCK_ID','NAME','PROPERTY_'.$this->arParams['PROPERTY_LINK']));

        while ($arSecondElements = $dbSecondsElements->Fetch()){
            $this->arResult["ITEMS"][$arSecondElements['PROPERTY_'.$this->arParams['PROPERTY_LINK'].'_VALUE']]['SUB_ITEMS'][] = $arSecondElements;
        }

        $this->arResult["NAV_STRING"] = $dbMainElements->GetPageNavStringEx(
            $navComponentObject,
            GetMessage('PAGINATOR_TITLE'),
            '',
            false,
            $this,
            array()
        );
        $this->arResult["NAV_CACHED_DATA"] = null;
        $this->arResult["NAV_RESULT"] = $dbMainElements;

        unset($arMainElementsIDs,$dbMainElements,$dbSecondsElements,$arSecondElements,$arMainElements);
    }

    private function getNaviGationParams (){
        $this->arNavParams = array(
            "nPageSize" => $this->arParams["SHOW_CNT_ELEMENT"],
            "bShowAll" => 'N',
        );
        $this->arNavigation = \CDBResult::GetNavParams($this->arNavParams);
    }

    private function getFilterData(){
        $this->arrFilterMain = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => $this->arParams['MAIN_IBLOCK_ID']
        );
        $this->arrFilterSecond = array(
            'ACTIVE' => 'Y',
            'IBLOCK_ID' => $this->arParams['SECOND_IBLOCK_ID'],
        );

        $arFilterCustomMain = $GLOBALS[$this->arParams['MAIN_FILTER_NAME']] ? : array();
        $arFilterCustomSecodn = $GLOBALS[$this->arParams['SECOND_FILTER_NAME']] ? : array();

        $this->arrFilterMain = array_merge($arFilterCustomMain,$this->arrFilterMain);
        $this->arrFilterSecond = array_merge($arFilterCustomSecodn,$this->arrFilterSecond);

        unset($arFilterCustomMain,$arFilterCustomSecodn);
    }

    protected function setCache($arParams)
    {
        return $this->startResultCache(false,$arParams);
    }

    private function showErrors()
    {
        foreach ($this->arrErrors as $arError) {
            echo "<pre>"; print_r($arError); echo "</pre>";
        }
    }

    public function executeComponent()
    {

        $this->arResult = array();
        $bErrors = !empty($this->arrErrors);

        if (!$bErrors) {
            $this->getFilterData();
            $this->getNaviGationParams();
            if($this->setCache(array($this->arNavigation, $this->arrFilterMain, $this->arrFilterSecond))){
                $this->getListOfElements();
                if (!empty($this->arrErrors)){
                    $this->abortResultCache();
                    $this->showErrors();
                } else {
                    $this->SetResultCacheKeys(array(
                        "ITEMS",
                        "NAV_CACHED_DATA",
                        "ITEMS_TIMESTAMP_X",
                    ));
                    $this->includeComponentTemplate();
                }
            }
        } else {
            $this->abortResultCache();
            $this->showErrors();
        }
    }
}