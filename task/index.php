<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тестовое задание");
?>
<?$APPLICATION->IncludeComponent(
    "is:filter.by.name",
    ".default",
    array(
    	"FILTERS_LABELS" => 'Название населенного пункта:, Название района:'
	),
    false
);?>
<?$APPLICATION->IncludeComponent(
	"is:complicated.list", 
	".default", 
	array(
		"COMPONENT_TEMPLATE" => ".default",
		"MAIN_IBLOCK_ID" => "6",
		"SECOND_IBLOCK_ID" => "7",
		"MAIN_FILTER_NAME" => "arrFilterMain",
		"SECOND_FILTER_NAME" => "arrFilterSecond",
		"PROPERTY_LINK" => "PUNKT_HREF",
		"SHOW_CNT_ELEMENT" => "3",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000"
	),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
